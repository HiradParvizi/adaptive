$(document).ready(function(){
  $('.lazy').lazy({
    // called before an elements gets handled
    beforeLoad: function(element) {
        var imageSrc = element.data('src');
        console.log('image "' + imageSrc + '" is about to be loaded');
    },
    
    // called after an element was successfully handled
    afterLoad: function(element) {
        var imageSrc = element.data('src');
        console.log('image "' + imageSrc + '" was loaded successfully');
    },
    
    // called whenever an element could not be handled
    onError: function(element) {
        var imageSrc = element.data('src');
        console.log('image "' + imageSrc + '" could not be loaded');
    }
  });
  
  // On click show deze methodes
  $(".buurtButton" ).click(function() {
    $(".phases").hide();

    

    // Removes currentPhase from old button and adds it to the clicked button
    $(".phaseButtons").removeClass("currentPhase");
    $(this).addClass("currentPhase");
    // Phase special colors
    $(".cookieButton").removeClass("zanding");
    $(".fallButton").removeClass("veluwe");
    $(".summerButton").removeClass("marina");

    // Background color changes
    $(".dynamicColor").removeClass("zanding");
    $(".dynamicColor").removeClass("veluweBG");
    $(".dynamicColor").removeClass("marinaBG");
    $(".dynamicColor").addClass("defaultDP");

    // Holiday resort image changes
    $(".droomparkImage").removeClass("zandingImage");
    $(".droomparkImage").removeClass("veluweImage");
    $(".droomparkImage").removeClass("marinaImage");
    $(".droomparkImage").addClass("locationImage");

    $(".locationPhase").show();
  });

  $(".cookieButton" ).click(function() {
    $(".phases").hide();
    
    $(".phaseButtons").removeClass("currentPhase");
    $(this).addClass("currentPhase");
    // Phase special colors
    $(this).addClass("zanding");
    $(".fallButton").removeClass("veluwe");
    $(".summerButton").removeClass("marina");

    $(".dynamicColor").removeClass("defaultDP");
    $(".dynamicColor").removeClass("veluweBG");
    $(".dynamicColor").removeClass("marinaBG");
    $(".dynamicColor").addClass("zanding");

    // Holiday resort image changes
    $(".droomparkImage").removeClass("locationImage");
    $(".droomparkImage").removeClass("veluweImage");
    $(".droomparkImage").removeClass("marinaImage");
    $(".droomparkImage").addClass("zandingImage");

    $(".cookiePhase").show();
  });

  $(".fallButton" ).click(function() {
    $(".phases").hide();
    
    $(".phaseButtons").removeClass("currentPhase");
    $(this).addClass("currentPhase");
    // Phase special colors
    $(this).addClass("veluwe");
    $(".cookieButton").removeClass("zanding");
    $(".summerButton").removeClass("marina");

    $(".dynamicColor").removeClass("defaultDP");
    $(".dynamicColor").removeClass("marinaBG");
    $(".dynamicColor").removeClass("zanding");
    $(".dynamicColor").addClass("veluweBG");

     // Holiday resort image changes
     $(".droomparkImage").removeClass("locationImage");
     $(".droomparkImage").removeClass("zandingImage");
     $(".droomparkImage").removeClass("marinaImage");
     $(".droomparkImage").addClass("veluweImage");

    $(".fallPhase").show();
  });

  $(".summerButton" ).click(function() {
    $(".phases").hide();
    
    $(".phaseButtons").removeClass("currentPhase");
    $(this).addClass("currentPhase");
    // Phase special colors
    $(this).addClass("marina");
    $(".cookieButton").removeClass("zanding");
    $(".fallButton").removeClass("veluwe");

    $(".dynamicColor").removeClass("defaultDP");
    $(".dynamicColor").removeClass("veluweBG");
    $(".dynamicColor").removeClass("zanding");
    $(".dynamicColor").addClass("marinaBG");

     // Holiday resort image changes
     $(".droomparkImage").removeClass("locationImage");
     $(".droomparkImage").removeClass("zandingImage");
     $(".droomparkImage").removeClass("veluweImage");
     $(".droomparkImage").addClass("marinaImage");

    $(".summerPhase").show();
  });

  $(".testButton" ).click(function() {
    $(".methods").hide();
    
    $(".phaseButtons").removeClass("currentPhase");
    $(this).addClass("currentPhase");

    $(".test-phase").show();
  });
});