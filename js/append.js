$(document).ready( function() {
var currentPrice = 0;
var currentTime = 0;
var currentValue = 0;
var currentComplexity = 0;
var aantalItems = 0;
var totalPrice = 0;
var totalValue = 0;
var totalComplexity = 0;
var count = 0;
var count2 = 0;
var count3 = 0;
var count4 = 0;
var count5 = 0;
var count6 = 0;
var count7 = 0;
var count8 = 0;
var count9 = 0;
var count10 = 0;

var removeClassPlus;
var addClassChecked;
var addClassOk;
var removeClassChecked;
var removeClassOk;
var addClassPlus;


				$.ajax({
				url:"intention.json",
				dataType: "json",
				success: function(data)
					{	
						$.each(data, function(index, item)
						{	
							function updateData(){
								// Round everything up
								currentValue = Math.round(currentValue);
								currentComplexity = Math.round(currentComplexity);

								totalValue = currentValue / aantalItems;
								totalComplexity = currentComplexity / aantalItems;
				
								// Totale prijs berekenen
								totalPrice = currentTime * 150;
								totalPrice = Math.round(totalPrice);

								// Totale waarde op 1 deciamalen zetten
								Cookies.set('totalComplexity', totalComplexity, { expires: 90 });
						    	Cookies.set('totalPrice', totalPrice,  { expires: 90 });
						    	Cookies.set('totalValue', totalValue.toFixed(1),  { expires: 90 });
						    	Cookies.set('currentTime', currentTime,  { expires: 90 });

								$(".currentTime, .tijdDashboard").html('<p class="animated bounceIn" av-animation="bounceIn"><span class="glyphicon glyphicon-time" aria-hidden="true"></span>' + Cookies.get('currentTime') + 'u</p>' );
								$(".currentPrice, .prijsDashboard").html('<p class="animated bounceIn" av-animation="bounceIn">&euro;' + Cookies.get('totalPrice') + ',00</p>');
								$(".currentValue, .waardeDashboard").html('<p class="animated bounceIn" av-animation="bounceIn"><span class="glyphicon glyphicon-heart" aria-hidden="true"></span>' + Cookies.get('totalValue') + '/10</p>' );
								
								if(Math.round(Cookies.get('totalComplexity')) > 2){
									$(".complexStars").html('<span class="glyphicon glyphicon-star animated bounceIn" av-animation="bounceIn" aria-hidden="true"></span><span class="glyphicon glyphicon-star animated bounceIn" av-animation="bounceIn" aria-hidden="true"></span><span class="glyphicon glyphicon-star animated bounceIn" av-animation="bounceIn" aria-hidden="true"></span>');
								}
								if(Math.round(Cookies.get('totalComplexity')) < 2){
									$(".complexStars").html('<span class="glyphicon glyphicon-star animated bounceIn" av-animation="bounceIn" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>');
								}
								if(Math.round(Cookies.get('totalComplexity')) == 2){
									$(".complexStars").html('<span class="glyphicon glyphicon-star animated bounceIn" av-animation="bounceIn" aria-hidden="true"></span><span class="glyphicon glyphicon-star animated bounceIn" av-animation="bounceIn" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>');
								}
							}

							for (var i = 1; i < 27; i++) {
								var idActive = $.cookie('activeId' + i);

							    if (idActive) {
							        $('#'+idActive).addClass('checked');
							        $('#'+idActive).find('.glyphicon').addClass('glyphicon-ok');
							    }
							    else{
							    	$('#'+idActive).removeClass('checked');
							        $('#'+idActive).find('.glyphicon').removeClass('glyphicon-ok');
							    }
						    }

							// On click moet die voor een bepaalde methode (alle) data toevoegen en aangeven hoeveel items zodat de formule nog klopt
							$(".addInterview" ).click(function() {
								count++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
    							if (isEven(count) === false) {
    								var id = $(this).attr('id');

								    removeClassPlus = $(this).find('.glyphicon').removeClass('glyphicon-plus');
									addClassChecked = $(this).addClass('checked');
									addClassOk = $(this).find('.glyphicon').addClass('glyphicon-ok');
									$.cookie('activeId1', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								    currentTime += item['interview']['tijdInUren'];
									currentPrice += item['interview']['geld'];
									currentValue += item['interview']['waarde'];
									currentComplexity += item['interview']['moeilijkheidsgraad'];
								}
								// on even clicks do this
								else if (isEven(count) === true) {
									removeClassChecked = $(this).removeClass('checked');
									removeClassOk = $(this).find('.glyphicon').removeClass('glyphicon-ok');
									addClassPlus = $(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId1' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								    aantalItems -= 1;
								    currentTime -= item['interview']['tijdInUren'];
									currentPrice -= item['interview']['geld'];
									currentValue -= item['interview']['waarde'];
									currentComplexity -= item['interview']['moeilijkheidsgraad'];
								}
								updateData();
							});

							$(".addChallenge" ).click(function() {
							   	count2++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count2) === false) {
							   		var id = $(this).attr('id');

									$(this).find('.glyphicon').removeClass('glyphicon-plus');
									$(this).addClass('checked');
									$(this).find('.glyphicon').addClass('glyphicon-ok');
									$.cookie('activeId2', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								    currentTime += item['designChallenge']['tijdInUren'];
									currentPrice += item['designChallenge']['geld'];
									currentValue += item['designChallenge']['waarde'];
									currentComplexity += item['designChallenge']['moeilijkheidsgraad'];
								}
								// on even clicks do this
								else if (isEven(count2) === true) {
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId2' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								    aantalItems -= 1;
								    currentTime -= item['designChallenge']['tijdInUren'];
									currentPrice -= item['designChallenge']['geld'];
									currentValue -= item['designChallenge']['waarde'];
									currentComplexity -= item['designChallenge']['moeilijkheidsgraad'];
								}
								updateData();
							});

							$(".addPlan" ).click(function() {
								count3++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count3) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId3', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								    currentTime += item['projectPlan']['tijdInUren'];
									currentPrice += item['projectPlan']['geld'];
									currentValue += item['projectPlan']['waarde'];
									currentComplexity += item['projectPlan']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count3) === true) {
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId3' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								    aantalItems -= 1;
								    currentTime -= item['projectPlan']['tijdInUren'];
									currentPrice -= item['projectPlan']['geld'];
									currentValue -= item['projectPlan']['waarde'];
									currentComplexity -= item['projectPlan']['moeilijkheidsgraad'];
								}
								updateData();
							});

							$(".addAudience" ).click(function() {
								count4++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count4) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId4', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								    currentTime += item['defineAudience']['tijdInUren'];
									currentPrice += item['defineAudience']['geld'];
									currentValue += item['defineAudience']['waarde'];
									currentComplexity += item['defineAudience']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count4) === true) {
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId4' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['defineAudience']['tijdInUren'];
									currentPrice -= item['defineAudience']['geld'];
									currentValue -= item['defineAudience']['waarde'];
									currentComplexity -= item['defineAudience']['moeilijkheidsgraad'];
								}
								updateData();								
							});

							$(".addExtremes" ).click(function() {
								count5++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count5) === false) {
							   		var id = $(this).attr('id');
								   	$(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								   	$.cookie('activeId5', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								    currentTime += item['extremes']['tijdInUren'];
									currentPrice += item['extremes']['geld'];
									currentValue += item['extremes']['waarde'];
									currentComplexity += item['extremes']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count5) === true) {
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId5' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['extremes']['tijdInUren'];
									currentPrice -= item['extremes']['geld'];
									currentValue -= item['extremes']['waarde'];
									currentComplexity -= item['extremes']['moeilijkheidsgraad'];
								}
								updateData();
								
							});

							$(".addRecruit" ).click(function() {
								count6++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count6) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								  	$.cookie('activeId6', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								    currentTime += item['recruiting']['tijdInUren'];
									currentPrice += item['recruiting']['geld'];
									currentValue += item['recruiting']['waarde'];
									currentComplexity += item['recruiting']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count6) === true) {
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId6' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['recruiting']['tijdInUren'];
									currentPrice -= item['recruiting']['geld'];
									currentValue -= item['recruiting']['waarde'];
									currentComplexity -= item['recruiting']['moeilijkheidsgraad'];
								}
								updateData();
							});

							$(".addEnquete" ).click(function() {
								count7++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count7) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId7', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								    currentTime += item['enquete']['tijdInUren'];
									currentPrice += item['enquete']['geld'];
									currentValue += item['enquete']['waarde'];
									currentComplexity += item['enquete']['moeilijkheidsgraad'];
								}
								
								// on even clicks do this
								else if (isEven(count7) === true) {
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId7' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['enquete']['tijdInUren'];
									currentPrice -= item['enquete']['geld'];
									currentValue -= item['enquete']['waarde'];
									currentComplexity -= item['enquete']['moeilijkheidsgraad'];
								}
								updateData();
							});

							$(".addCard" ).click(function() {
								count8++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count8) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId8', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								    currentTime += item['card']['tijdInUren'];
									currentPrice += item['card']['geld'];
									currentValue += item['card']['waarde'];
									currentComplexity += item['card']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count8) === true) {
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId8' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['card']['tijdInUren'];
									currentPrice -= item['card']['geld'];
									currentValue -= item['card']['waarde'];
									currentComplexity -= item['card']['moeilijkheidsgraad'];
								}
								updateData();
							});

							
							$(".addCollage" ).click(function() {
								count9++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count9) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId9', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								    currentTime += item['collage']['tijdInUren'];
									currentPrice += item['collage']['geld'];
									currentValue += item['collage']['waarde'];
									currentComplexity += item['collage']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count9) === true) {
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId9' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['collage']['tijdInUren'];
									currentPrice -= item['collage']['geld'];
									currentValue -= item['collage']['waarde'];
									currentComplexity -= item['collage']['moeilijkheidsgraad'];
								}
								updateData();
							});

							$(".addAnaloog" ).click(function() {
								count10++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count10) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId10', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								   	currentTime += item['analoog']['tijdInUren'];
									currentPrice += item['analoog']['geld'];
									currentValue += item['analoog']['waarde'];
									currentComplexity += item['analoog']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count10) === true) {
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId10' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['analoog']['tijdInUren'];
									currentPrice -= item['analoog']['geld'];
									currentValue -= item['analoog']['waarde'];
									currentComplexity -= item['analoog']['moeilijkheidsgraad'];
								}
								updateData();
							});

							// Define phase
							var count11 = 0;
							$(".addPersona" ).click(function() {
								count11++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count11) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId11', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								   	currentTime += item['persona']['tijdInUren'];
									currentPrice += item['persona']['geld'];
									currentValue += item['persona']['waarde'];
									currentComplexity += item['persona']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count11) === true) {
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId11' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['persona']['tijdInUren'];
									currentPrice -= item['persona']['geld'];
									currentValue -= item['persona']['waarde'];
									currentComplexity -= item['persona']['moeilijkheidsgraad'];
								}
								updateData();
							});

							var count12 = 0;
							$(".addEmpathyMap" ).click(function() {
								count12++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count12) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId12', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								   	currentTime += item['empathyMap']['tijdInUren'];
									currentPrice += item['empathyMap']['geld'];
									currentValue += item['empathyMap']['waarde'];
									currentComplexity += item['empathyMap']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count12) === true) {
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId12' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['empathyMap']['tijdInUren'];
									currentPrice -= item['empathyMap']['geld'];
									currentValue -= item['empathyMap']['waarde'];
									currentComplexity -= item['empathyMap']['moeilijkheidsgraad'];
								}
								updateData();
							});

							var count13 = 0;
							$(".addBusiness" ).click(function() {
								count13++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count13) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId13', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								   	currentTime += item['businessCanvas']['tijdInUren'];
									currentPrice += item['businessCanvas']['geld'];
									currentValue += item['businessCanvas']['waarde'];
									currentComplexity += item['businessCanvas']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count13) === true) {
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId13' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['businessCanvas']['tijdInUren'];
									currentPrice -= item['businessCanvas']['geld'];
									currentValue -= item['businessCanvas']['waarde'];
									currentComplexity -= item['businessCanvas']['moeilijkheidsgraad'];
								}
								updateData();
							});

							var count14 = 0;
							$(".addThemes" ).click(function() {
								count14++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count14) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId14', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								   	currentTime += item['themas']['tijdInUren'];
									currentPrice += item['themas']['geld'];
									currentValue += item['themas']['waarde'];
									currentComplexity += item['themas']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count14) === true) {
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId14' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['themas']['tijdInUren'];
									currentPrice -= item['themas']['geld'];
									currentValue -= item['themas']['waarde'];
									currentComplexity -= item['themas']['moeilijkheidsgraad'];
								}
								updateData();
							});

							// Ideate phase
							var count15 = 0;
							$(".addCoCreation" ).click(function() {
								count15++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count15) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId15', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								   	currentTime += item['coCreatie']['tijdInUren'];
									currentPrice += item['coCreatie']['geld'];
									currentValue += item['coCreatie']['waarde'];
									currentComplexity += item['coCreatie']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count15) === true) {
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId15' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['coCreatie']['tijdInUren'];
									currentPrice -= item['coCreatie']['geld'];
									currentValue -= item['coCreatie']['waarde'];
									currentComplexity -= item['coCreatie']['moeilijkheidsgraad'];
								}
								updateData();
							});

							var count16 = 0;
							$(".addBrainstorm" ).click(function() {
								count16++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count16) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId16', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								   	currentTime += item['brainstormen']['tijdInUren'];
									currentPrice += item['brainstormen']['geld'];
									currentValue += item['brainstormen']['waarde'];
									currentComplexity += item['brainstormen']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count16) === true) {
									var id = $(this).attr('id');
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId16' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['brainstormen']['tijdInUren'];
									currentPrice -= item['brainstormen']['geld'];
									currentValue -= item['brainstormen']['waarde'];
									currentComplexity -= item['brainstormen']['moeilijkheidsgraad'];
								}
								updateData();
							});

							var count17 = 0;
							$(".addBundle" ).click(function() {
								count17++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count17) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId17', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								   	currentTime += item['bundel']['tijdInUren'];
									currentPrice += item['bundel']['geld'];
									currentValue += item['bundel']['waarde'];
									currentComplexity += item['bundel']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count17) === true) {
									var id = $(this).attr('id');
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId17' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['bundel']['tijdInUren'];
									currentPrice -= item['bundel']['geld'];
									currentValue -= item['bundel']['waarde'];
									currentComplexity -= item['bundel']['moeilijkheidsgraad'];
								}
								updateData();
							});

							var count18 = 0;
							$(".addTopVijf" ).click(function() {
								count18++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count18) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId18', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								   	currentTime += item['topVijf']['tijdInUren'];
									currentPrice += item['topVijf']['geld'];
									currentValue += item['topVijf']['waarde'];
									currentComplexity += item['topVijf']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count18) === true) {
									var id = $(this).attr('id');
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId18' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['topVijf']['tijdInUren'];
									currentPrice -= item['topVijf']['geld'];
									currentValue -= item['topVijf']['waarde'];
									currentComplexity -= item['topVijf']['moeilijkheidsgraad'];
								}
								updateData();
							});

							var count19 = 0;
							$(".addGut" ).click(function() {
								count19++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count19) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId19', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								   	currentTime += item['gutCheck']['tijdInUren'];
									currentPrice += item['gutCheck']['geld'];
									currentValue += item['gutCheck']['waarde'];
									currentComplexity += item['gutCheck']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count19) === true) {
									var id = $(this).attr('id');
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId19' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['gutCheck']['tijdInUren'];
									currentPrice -= item['gutCheck']['geld'];
									currentValue -= item['gutCheck']['waarde'];
									currentComplexity -= item['gutCheck']['moeilijkheidsgraad'];
								}
								updateData();
							})

							var count20 = 0;
							$(".addConcepting" ).click(function() {
								count20++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count20) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId20', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								   	currentTime += item['concepting']['tijdInUren'];
									currentPrice += item['concepting']['geld'];
									currentValue += item['concepting']['waarde'];
									currentComplexity += item['concepting']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count20) === true) {
									var id = $(this).attr('id');
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId20' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['concepting']['tijdInUren'];
									currentPrice -= item['concepting']['geld'];
									currentValue -= item['concepting']['waarde'];
									currentComplexity -= item['concepting']['moeilijkheidsgraad'];
								}
								updateData();
							})

							// Prototype phase
							var count21 = 0;
							$(".addBepaal" ).click(function() {
								count21++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count21) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId21', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								   	currentTime += item['bepaal']['tijdInUren'];
									currentPrice += item['bepaal']['geld'];
									currentValue += item['bepaal']['waarde'];
									currentComplexity += item['bepaal']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count21) === true) {
									var id = $(this).attr('id');
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId21' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['bepaal']['tijdInUren'];
									currentPrice -= item['bepaal']['geld'];
									currentValue -= item['bepaal']['waarde'];
									currentComplexity -= item['bepaal']['moeilijkheidsgraad'];
								}
								updateData();
							})

							var count22 = 0;
							$(".addStory" ).click(function() {
								count22++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count22) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId22', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								   	currentTime += item['storyboard']['tijdInUren'];
									currentPrice += item['storyboard']['geld'];
									currentValue += item['storyboard']['waarde'];
									currentComplexity += item['storyboard']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count22) === true) {
									var id = $(this).attr('id');
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId22' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['storyboard']['tijdInUren'];
									currentPrice -= item['storyboard']['geld'];
									currentValue -= item['storyboard']['waarde'];
									currentComplexity -= item['storyboard']['moeilijkheidsgraad'];
								}
								updateData();
							})

							var count23 = 0;
							$(".addRapid" ).click(function() {
								count23++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count23) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId23', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								   	currentTime += item['rapid']['tijdInUren'];
									currentPrice += item['rapid']['geld'];
									currentValue += item['rapid']['waarde'];
									currentComplexity += item['rapid']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count23) === true) {
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId23' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['rapid']['tijdInUren'];
									currentPrice -= item['rapid']['geld'];
									currentValue -= item['rapid']['waarde'];
									currentComplexity -= item['rapid']['moeilijkheidsgraad'];
								}
								updateData();
							})

							// Test phase
							var count24 = 0;
							$(".addJourney" ).click(function() {
								count24++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count24) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId24', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								   	currentTime += item['journey']['tijdInUren'];
									currentPrice += item['journey']['geld'];
									currentValue += item['journey']['waarde'];
									currentComplexity += item['journey']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count24) === true) {
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId24' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['journey']['tijdInUren'];
									currentPrice -= item['journey']['geld'];
									currentValue -= item['journey']['waarde'];
									currentComplexity -= item['journey']['moeilijkheidsgraad'];
								}
								updateData();
							})

							var count25 = 0;
							$(".addFeedback" ).click(function() {
								count25++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count25) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId25', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								   	currentTime += item['feedback']['tijdInUren'];
									currentPrice += item['feedback']['geld'];
									currentValue += item['feedback']['waarde'];
									currentComplexity += item['feedback']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count25) === true) {
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId25' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['feedback']['tijdInUren'];
									currentPrice -= item['feedback']['geld'];
									currentValue -= item['feedback']['waarde'];
									currentComplexity -= item['feedback']['moeilijkheidsgraad'];
								}
								updateData();
							})

							var count26 = 0;
							$(".addUserTests" ).click(function() {
								count26++;
							    //even odd click detect 
							    var isEven = function(someNumber) {
							        return (someNumber % 2 === 0) ? true : false;
							    };

							    // on odd clicks do this
							   	if (isEven(count26) === false) {
							   		var id = $(this).attr('id');
								    $(this).find('.glyphicon').removeClass('glyphicon-plus');
								    $(this).addClass('checked');
								    $(this).find('.glyphicon').addClass('glyphicon-ok');
								    $.cookie('activeId26', id, { expires: 90}); //Save anchor id as cookie

								    aantalItems += 1;
								   	currentTime += item['userTests']['tijdInUren'];
									currentPrice += item['userTests']['geld'];
									currentValue += item['userTests']['waarde'];
									currentComplexity += item['userTests']['moeilijkheidsgraad'];
								}

								// on even clicks do this
								else if (isEven(count26) === true) {
									$(this).removeClass('checked');
									$(this).find('.glyphicon').removeClass('glyphicon-ok');
									$(this).find('.glyphicon').addClass('glyphicon-plus');
									document.cookie = 'activeId26' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

								   	aantalItems -= 1;
								    currentTime -= item['userTests']['tijdInUren'];
									currentPrice -= item['userTests']['geld'];
									currentValue -= item['userTests']['waarde'];
									currentComplexity -= item['userTests']['moeilijkheidsgraad'];
								}
								updateData();
							})

							$(".confirmButton" ).click(function() {
								updateData();
								// Here it should store which methods were picked and it should store the final data which was collected here :)
							});
						});
					}
				}
				);
});