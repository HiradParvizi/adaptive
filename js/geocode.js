function geocode(query){
    $.ajax({
      url: 'https://api.opencagedata.com/geocode/v1/json',
      method: 'GET',
      data: {
        'key': '082101601810400ea7270e520c78e5e6',
        'q': query,
        'no_annotations': 1
        // see other optional params:
        // https://opencagedata.com/api#forward-opt
      },
      dataType: 'json',
      statusCode: {
        200: function(response){  // success
          console.log(response.results[0].formatted);
        },
        402: function(){
          console.log('hit free-trial daily limit');
          console.log('become a customer: https://opencagedata.com/pricing');
        }
        // other possible response codes:
        // https://opencagedata.com/api#codes
      }
    });
  }
 
 

$(".buurtButton" ).click(function() {

   window.navigator.geolocation.getCurrentPosition(response => {
    // Set variables for distance
    var myPositionA = response.coords.latitude;
    var myPositionB = response.coords.longitude;
    var marinaA = '52.035673';
    var marinaB = '6.1339598';
    var spaarnwoudeA = '52.3944357';
    var spaarnwoudeB = '4.7540229';
    var buitenhuizenA = '52.46';
    var buitenhuizenB = '4.65';

    var marinaAfstand = getDistanceFromLatLonInKm(myPositionA,myPositionB,marinaA,marinaB).toFixed(1);
    var spaarnwoudeAfstand = getDistanceFromLatLonInKm(myPositionA,myPositionB,spaarnwoudeA,spaarnwoudeB).toFixed(1);
    var buitenhuizenAfstand = getDistanceFromLatLonInKm(myPositionA,myPositionB,buitenhuizenA,buitenhuizenB).toFixed(1);

    $(".afstandCijfer").show();
    $(".afstandMarina").html('<span class="wegIcon glyphicon glyphicon-road"></span>' + marinaAfstand +' km');
    $(".afstandSpaarnwoude").html('<span class="wegIcon glyphicon glyphicon-road"></span>' + spaarnwoudeAfstand +' km');
    $(".afstandBuitenhuizen").html('<span class="wegIcon glyphicon glyphicon-road"></span>' + buitenhuizenAfstand +' km');
   }, console.log);
    
    // Calculates distance
    function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = deg2rad(lat2-lat1);  // deg2rad below
        var dLon = deg2rad(lon2-lon1); 
        var a = 
            Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
            Math.sin(dLon/2) * Math.sin(dLon/2)
            ; 
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
            var d = R * c; // Distance in km
            return d;
        }

    // Math
    function deg2rad(deg) {
        return deg * (Math.PI/180)
    }

});

